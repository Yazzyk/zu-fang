package config

import (
	"github.com/BurntSushi/toml"
	jsoniter "github.com/json-iterator/go"
	"github.com/paleblueyk/logger"
	"gitee.com/Yazzyk/zu-fang/pkg/fileutils"
)

type appConf struct {
	Douban douban `toml:"Douban"`
}

type douban struct {
	Dbcl2   string `toml:"dbcl2"`    // cookie
	TopicID uint   `toml:"topic_id"` // 话题id
}

var App appConf

// InitConf 初始化配置文件
func InitConf(env string) bool {
	var configPath string
	switch env {
	case "dev":
		configPath = "config/config_dev.toml"
	case "test":
		configPath = "config/config_test.toml"
	default:
		configPath = "config/config.toml"
		if !fileutils.FileIsExist(configPath) {
			configPath = "config.toml"
		}
	}
	if _, err := toml.DecodeFile(configPath, &App); err != nil {
		logger.Error(err)
		return false
	}
	logger.Info(jsoniter.MarshalToString(App))
	return true
}
