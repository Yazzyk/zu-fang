package model

import "time"

type Talk struct {
	//ID              uint      `json:"id"`
	Title           string     `json:"title" gorm:"comment:'标题'"`
	Link            string     `json:"link"` // 链接
	Author          string     `json:"author" gorm:"comment:'作者'"`
	Comment         int        `json:"comment" gorm:"comment:'评论/回应'"`
	LastCommentTime *time.Time `json:"last_comment_time" gorm:"comment:'最后评论时间'"`
}
