package main

import (
	"github.com/paleblueyk/logger"
	"gitee.com/Yazzyk/zu-fang/config"
	"gitee.com/Yazzyk/zu-fang/server"
	"os"
)

func main() {
	env := "prod"
	if len(os.Args) > 1 {
		env = os.Args[1]
	}
	if !config.InitConf(env) {
		logger.Error("读取配置文件失败")
		return
	}
	server.DoubanSpider()
}
